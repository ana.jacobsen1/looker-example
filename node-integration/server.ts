import express from "express";
import bodyParser from "body-parser";
//import { LookerNodeSDK  } from '@looker/sdk-node'
import { createSignedEmbedUrl, LookerEmbedUser } from "./auth_utils";

var app = express();
const LOOKER_HOST = 'zenklub.cloud.looker.com'

app.use(bodyParser.json());

var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

const port = process.env['PORT'] || 8080;

var server = app.listen(port, function () {
    console.log("App now running on port", port);
});

app.get("/api/status", function (req, res) {
    res.status(200).json({ status: "UP" });
});


app.get("/api/embed", async (req, res) => {

    const embedUrl = req.query.src.toString();
    console.log('Embed URL front', embedUrl);


    const time_session = 45 * 60;
    const secret = '639f3a77f92c7dcedbf92ea637634528f674a652c1cd941bbc5ac307b59a2ecf';

    //usuario no looker : https://zenklub.cloud.looker.com/admin/next/users
    const user: LookerEmbedUser = {
        external_user_id: '60539a991983ff1436fb6e89',

        first_name: 'Embeded',
        last_name: 'User',
        force_logout_login: true,
        session_length: time_session,
        permissions: ['see_user_dashboards', 'access_data', 'see_looks', 'download_with_limit', 'see_lookml_dashboards'],
        models: ['main'],
        user_attributes: {
            id_professional: '60539a991983ff1436fb6e89',
            date_session: '11',
            "ID do Profissional": "60539a991983ff1436fb6e89",
            professional_id: '60539a991983ff1436fb6e89',
            cl_pii: 'yes',
            pro_pii: 'yes'
        }
    }

    const signedUrl = createSignedEmbedUrl(
        embedUrl,
        user,
        LOOKER_HOST,
        secret
    )

    console.log('signed url  ', signedUrl);
    res.send({
        'url': signedUrl
    })
});

// app.get("/api/query", async (req, res) => {
//     const sdk = LookerNodeSDK.init31()
//     const data = await sdk.ok(sdk.all_dashboards())

//     //console.log('data', data);
//     res.send({
//         data
//     })
// });