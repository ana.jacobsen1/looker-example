import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home/home-page.component';
import { LookerPageComponent } from './pages/looker/looker-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'looker', component: LookerPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
