import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class APIService {
  constructor(private http: HttpClient) { }

  getEmbedUrl = (url: string) => {
    const domain = 'http://localhost:4200'
    const embededDomain = `${url}${url.includes('?') ? '&' : '?'}embed_domain=${encodeURIComponent(domain)}&sdk=2`
    return this.http.get<{url: string}>(`http://localhost:4200/api/embed?src=${encodeURIComponent(embededDomain)}`);
  }
}