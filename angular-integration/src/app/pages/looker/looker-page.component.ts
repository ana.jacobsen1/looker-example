import { Component, Inject, OnInit } from '@angular/core';
import {LookerEmbedSDK} from '@looker/embed-sdk';
import { DOCUMENT } from '@angular/common';

import { APIService } from 'src/app/services/api.service';

@Component({
  selector: 'looker-home-page',
  templateUrl: './looker-page.component.html',
  styleUrls: ['./looker-page.component.scss']
})
export class LookerPageComponent implements OnInit {
  title = 'Looker Example'
  url: string = '';
  dashboard = 55;

  constructor(@Inject(DOCUMENT) private document: Document,
  private apiService: APIService) { }

  ngOnInit(): void {
    // LookerEmbedSDK.init(lookerHost, '/api/auth')

    // const setupDashboard = (dashboard: any) => {
    //   this.document.querySelector('#run')?.addEventListener('click', () => {
    //     dashboard.send('dashboard:run')
    //   })
    // }
    
    // LookerEmbedSDK.createDashboardWithId(5)
    //   .appendTo('#dashboard')
    //   // .on('dashboard:run:start',
    //   //     () => updateState('#dashboard-state', 'Running')
    //   // )
    //   // .on('dashboard:run:complete',
    //   //     () => updateState('#dashboard-state', 'Done')
    //   // )
    //   .build()
    //   .connect()
    //   .then(setupDashboard)
    //   .catch((error: Error) => {
    //     console.error('An unexpected error occurred', error)
    //   })
      this.apiService.getEmbedUrl(`/embed/dashboards/${this.dashboard}`)
      .subscribe((embedUrl) => {
        this.url = embedUrl.url;
      })
  }

}