import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home/home-page.component';
import { LookerPageComponent } from './pages/looker/looker-page.component';
import { SafePipe } from './pipes/safe.pipe';
import { APIService } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LookerPageComponent,

    // pipes
    SafePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
